import  app from "./app.js";

const PORT=5000;

app.listen(PORT, function () {
  console.log("App listening on port 5000!");
});
