import express from "express";

var app = express();
app.use(express.json());

const students = ["Elie", "Matt", "Joel", "Michael"];

app.get("/", (req, res) => {
  return res.json(students);
});


export default app;
